# Copyright (C) 2011 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# This file is generated by device/common/generate-blob-lists.sh - DO NOT EDIT

# Bin
/system/bin/ATFWD-daemon
/system/bin/diag_callback_client
/system/bin/diag_klog
/system/bin/diag_mdlog
/system/bin/diag_socket_log
/system/bin/diag_uart_log
/system/bin/efsks
/system/bin/fmconfig
/system/bin/fm_qsoc_patches
/system/bin/gsiff_daemon
/system/bin/ks
/system/bin/mm-pp-daemon
/system/bin/mm-qcamera-daemon
/system/bin/mpdecision
/system/bin/netmgrd
/system/bin/nl_listener
/system/bin/qcks
/system/bin/qmuxd
/system/bin/qseecomd
/system/bin/radish
/system/bin/rmt_storage
/system/bin/sensors.qcom
/system/bin/thermald
/system/bin/thermal-engine
/system/bin/time_daemon
/system/bin/usbhub
/system/bin/usbhub_init
/system/bin/v4l2-qcamera-app

/system/bin/irsc_util

# ETC
/system/etc/modem/Diag.cfg
/system/etc/firmware/vidc.b00
/system/etc/firmware/vidc.b01
/system/etc/firmware/vidc.b02
/system/etc/firmware/vidc.b03
/system/etc/firmware/vidc.mdt
/system/etc/firmware/vidc_1080p.fw
/system/etc/firmware/vidcfw.elf

# Bt
/system/bin/btnvtool
/system/bin/hci_qcomm_init
/system/lib/libbt-vendor.so
/system/vendor/lib/libbtnv.so


# Light
/system/lib/hw/lights.msm8960.so

# Sensors
/system/lib/hw/sensors.msm8960.so
/system/lib/libAKM.so
/system/lib/libsensor1.so
/system/lib/libsensor_reg.so
/system/lib/libsensor_test.so
/system/lib/libsensor_user_cal.so

# Camera
/system/lib/hw/camera.msm8960.so
/system/lib/libgemini.so
/system/lib/libimage-jpeg-dec-omx-comp.so
/system/lib/libimage-jpeg-enc-omx-comp.so
/system/lib/libimage-omx-common.so
/system/lib/libmmcamera_faceproc.so
/system/lib/libmmcamera_frameproc.so
/system/lib/libmmcamera_hdr_lib.so
/system/lib/libmmcamera_image_stab.so
/system/lib/libmmcamera_interface2.so
/system/lib/libmmcamera_statsproc31.so
/system/lib/libmmcamera_wavelet_lib.so
/system/lib/libmmjpeg.so
/system/lib/libmmstillomx.so
/system/lib/liboemcamera.so
/system/lib/libstlport.so


# GPS
/system/lib/hw/flp.msm8960.so
/system/lib/hw/gps.msm8960.so
/system/lib/libulp2.so
/system/lib/libloc_core.so
/system/lib/libloc_eng.so
/system/lib/libgps.utils.so
/system/vendor/lib/libgeofence.so
/system/vendor/lib/libizat_core.so
/system/vendor/lib/libloc_api_v02.so
/system/vendor/lib/libloc_ds_api.so

# Audio
/system/lib/libsurround_proc.so

# OMX
/system/lib/libOmxAacDec.so
/system/lib/libOmxEvrcDec.so
/system/lib/libOmxMux.so
/system/lib/libOmxQcelp13Dec.so
/system/lib/libOmxWmaDec.so

/system/lib/libI420colorconvert.so
/system/lib/libmm-color-convertor.so
/system/lib/libmmipl.so
/system/lib/libmmjps.so
/system/lib/libmmmpo.so
/system/lib/libmmmpod.so

/system/lib/libCommandSvc.so
/system/lib/libconfigdb.so
/system/lib/libdsi_netctrl.so
/system/lib/libdsprofile.so
/system/lib/libdss.so
/system/lib/libdsucsd.so
/system/lib/libdsutils.so
/system/lib/libidl.so
/system/lib/libnetmgr.so
/system/lib/libqcci_adc.so
/system/lib/libqcci_legacy.so
/system/lib/libqdi.so
/system/lib/libqdp.so
/system/lib/libqmi.so
/system/lib/libqmi_client_qmux.so
/system/lib/libqmi_csvt_srvc.so
/system/lib/libqmiservices.so
/system/lib/libril-qc-qmi-1.so
/system/lib/libril-qcril-hook-oem.so
/system/lib/libxml.so

/system/vendor/lib/libdiag.so
/system/vendor/lib/libqmi_cci.so
/system/vendor/lib/libqmi_common_so.so
/system/vendor/lib/libqmi_csi.so
/system/vendor/lib/libqmi_encdec.so

/system/lib/drm/libdrmprplugin.so
/system/lib/libDivxDrm.so
/system/lib/libdrmdiag.so
/system/lib/libdrmfs.so
/system/lib/libdrmtime.so
/system/lib/libQSEEComAPI.so
/system/lib/libSHIMDivxDrm.so

/system/lib/libthermalclient.so

/system/lib/libadsprpc.so

/system/vendor/lib/libalarmservice_jni.so

/system/vendor/lib/hw/power.qcom.so
