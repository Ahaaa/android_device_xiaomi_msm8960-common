#!/bin/sh

VENDOR=xiaomi
DEVICE=msm8960-common

BASE=../../../vendor/$VENDOR/$DEVICE/proprietary
rm -rf $BASE/*

for FILE in `cat proprietary-blobs.txt | grep -v ^# | grep -v ^$ | sed -e 's#^/system/##g'`; do
    DIR=`dirname $FILE`
    if [ ! -d $BASE/$DIR ]; then
        mkdir -p $BASE/$DIR
    fi
    adb pull /system/$FILE $BASE/$FILE
done

# some extra stuff
adb pull /system/app/TimeService/TimeService.apk $BASE/app/TimeService/TimeService.apk
adb pull /system/vendor/lib/libTimeService.so $BASE/lib/libTimeService.so
adb pull /system/vendor/lib/libtime_genoff.so $BASE/vendor/lib/libtime_genoff.so

adb pull /system/lib/libsurround_proc.so $BASE/lib/libsurround_proc.so
adb pull /system/lib/libqminvapi.so $BASE/lib/libqminvapi.so
adb pull /system/vendor/lib/libqti-perfd-client.so $BASE/vendor/lib/libqti-perfd-client.so
adb pull /system/vendor/lib/libqc-opt.so $BASE/vendor/lib/libqc-opt.so

./setup-makefiles.sh
